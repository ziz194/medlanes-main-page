<?php 
$req = explode('=', $_SERVER['REQUEST_URI']);
$cnt = count($req);
if(!($cnt <= 1)){
  	switch ($req[$cnt-1]) {
  		case 'Berlin':
  		case 'Bielefeld':
  		case 'Bochum':
  		case 'Bonn':
  		case 'Bremen':
  		case 'Cologne':
  		case 'Dortmund':
  		case 'Dresden':
  		case 'Duesseldorf':
  		case 'Duisburg':
  		case 'Essen':
  		case 'Frankfurt':
  		case 'Hamburg':
  		case 'Hanover':
  		case 'Leipzig':
  		case 'Mannheim':
  		case 'Munich':
  		case 'Nuremberg':
  		case 'Stuttgart':
  		case 'Wuppertal': {
  			$city = $req[$cnt-1];
  			break;
  		}
  		default: {
  			$city = 'Berlin';
  		}
  	}
  	$imgUrl = $city.".jpg";
}
?>
<!DOCTYPE html>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>

<!--- Basic Page Needs
   ================================================== -->
<meta charset="utf-8">
<title>Medlanes - Hausärzte kommen zu Ihnen - rund um die Uhr.</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Mobile Specific Metas
   ================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
   ================================================== -->

<link rel="stylesheet" href="css/base.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/responsive/media-queries.css">
<link rel="stylesheet" href="css/animate/animate.css">
<link rel="stylesheet" href="css/bxslider/jquery.bxslider.css" type="text/css" />

<!-- Script
   =================================================== -->
<!-- <script src="js/modernizr.js"></script> -->
<script type="text/javascript" async>

// function loadScript( url, callback ) {
//   var script = document.createElement( "script" )
//   script.type = "text/javascript";
//   if(script.readyState) {  //IE
//     script.onreadystatechange = function() {
//       if ( script.readyState === "loaded" || script.readyState === "complete" ) {
//         script.onreadystatechange = null;
//         callback();
//       }
//     };
//   } else {  //Others
//     script.onload = function() {
//       callback();
//     };
//   }

//   script.src = url;
//   document.getElementsByTagName( "head" )[0].appendChild( script );
// }

// function updateCity(){
// 	if(realCity!='Berlin'){
// 		location.replace('?='+realCity);
// 	}
// }

// if(window.location.href.split('=').length<2 || window.location.href.split('=')[1]==''){
// 	// call the function...
// 	loadScript("https://js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js", function() {
// 	  	var onGeoSuccess = function (geoipResponse) {
// 	  		console.log(geoipResponse);
// 	        if(geoipResponse.city.names.en){
// 	          	newCity = geoipResponse.city.names.en;
// 	          	switch(newCity){
// 	          		case 'Berlin':
// 	          		case 'Bielefeld':
// 	          		case 'Bochum':
// 	          		case 'Bonn':
// 	          		case 'Bremen':
// 	          		case 'Cologne':
// 	          		case 'Dortmund':
// 	          		case 'Dresden':
// 	          		case 'Duesseldorf':
// 	          		case 'Duisburg':
// 	          		case 'Essen':
// 	          		case 'Frankfurt':
// 	          		case 'Hamburg':
// 	          		case 'Hanover':
// 	          		case 'Leipzig':
// 	          		case 'Mannheim':
// 	          		case 'Munich':
// 	          		case 'Nuremberg':
// 	          		case 'Stuttgart':
// 	          		case 'Wuppertal': {
// 	          			realCity = newCity;
// 	          		}
// 	          		default: {
// 	          			realCity='Berlin';
// 	          		}
// 	          	}
// 	          	updateCity();
// 	        }
// 	    };

// 	    var onGeoError = function (error) {
// 	        realCity= 'Berlin';
// 	        updateCity();
// 	        return;
// 	    };

// 	    if(geoip2){
// 	        geoip2.city(onGeoSuccess, onGeoError);
// 	    }else{
// 	        realCity = "Berlin";
// 	        updateCity();
// 	    }
// 	});
// };

</script>

<!-- Favicons
	=================================================== -->
<link rel="shortcut icon" href="images/favicon.png" >
</head>

<body class="homepage">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NSL2DD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NSL2DD');</script>
<!-- End Google Tag Manager -->



<!-- Header
   =================================================== -->
<header id="main-header">
  <div class="row header-inner">
    <div class="logo"> <a class="smoothscroll" href="/livechat/index.html"></a> </div>
    <nav id="nav-wrap" class="pull-right"> <a class="mobile-btn" href="#nav-wrap" title="Show navigation"> <span class='menu-text'>Show Menu</span> <span class="menu-icon"></span> </a> <a class="mobile-btn" href="#" title="Hide navigation"> <span class='menu-text'>Hide Menu</span> <span class="menu-icon"></span> </a>
      <ul id="nav" class="nav">
        <li class="current"><a class="smoothscroll" href="#about">So funkioniert's</a></li>
        <li><a class="smoothscroll" href="#doctors">Über Uns</a></li>
        <li><a class="smoothscroll" href="#support">FAQ</a></li>
        <li><a class="smoothscroll" href="#newsLetter">Vorteile</a></li>
        <li><a class="smoothscroll" href="#contact">Kontakt</a></li>
        <li><a href="contact.html" class="button">Jetzt Arzt Kontaktieren</a></li>
      </ul>
    </nav>
    <!-- /nav-wrap --> 
    
  </div>
  <!-- /header-inner --> 
  
</header>

<!-- Hero
   =================================================== -->
<section id="hero" class="indexpage" <?php 
  echo 'style="background-image:url(';
  if(isset($imgUrl)){
    echo "images/country/".$imgUrl;
  } else{
    echo "./images/country/berlin.jpg";
  } 
  echo ')"';?>>
  <div class="row hero-content">
    <div class="twelve columns flex-container">
      <div id="hero-slider" class="flexslider">
        <ul class="slides">
          <!-- Slide -->
          <li>
            <div class="flex-caption">
              <h1>Brauchen Sie einen Arzt in<span id="country">&nbsp;<?php if(isset($city)){
                echo $city;
              }else{
                echo "Berlin";
              } ?></span>?</h1>
              <h3>Jetzt anrufen und wir sind in 10 Minuten unterwegs zu Ihnen!</h3>
              <h5>Untersuchungen, Krankschreibungen, Rezepte & mehr<br>bequem von zu Hause für nur 5€ Zuzahlung!</h5>

              <div class="ctabutton">
              <a href="contact.html" class="button orange">Arzt Kontaktieren</a>
              </div>
</div>
          </li>
        </ul>
      </div>
      <!-- .flexslider --> 
      
    </div>
    <h1 id="index-about"><a class="smoothscroll" href="#about">&nbsp;</a></h1>
	    <!-- .flex-container --> 
    
  </div>
  <!-- .hero-content 
  <a class="smoothscroll polyarrow" title="Back to Top" href="#about"><img src="images/icons/polygon.png" alt=""/> </a>-->
</section>
<!-- #hero --> 

<!-- About Section
   ================================================== -->
<section id="about">
  <div class="row section-head">
    <h2>So funkioniert's</h2>
  </div>
  <div class="row">
    <div class="twelve columns">
      <div class="service-list bgrid-third s-bgrid-half mob-bgrid-whole text-left">
        <div class="bgrid wow animated fadeInLeft">
          <div>
            <img src="images/how--it-done-1.jpg">
          </div>
          <h3>Senden Sie Ihre Anfrage</h3>
          <div class="service-content">
            <p>Bitte nennen Sie einige wichtige Informationen, damit sich unser Arzt auf Ihren Besuch vorbereiten kann.</p>
          </div>
        </div>
        <!-- /bgrid -->
        
        <div class="bgrid wow animated bounceInUp">
          <div>
            <img src="images/how--it-done-2.jpg">
          </div>          
          <h3>Bezahlen Sie online oder vor Ort</h3>
          <div class="service-content">
            <p>Für nur 5€ Zuzahlung sind wir schnellstmöglich bei Ihnen. Bezahlen Sie bequem vor Ort oder online per Kreditkarte oder PayPal. </p>
          </div>
        </div>
        <!-- /bgrid -->
        
        <div class="bgrid wow animated fadeInRight">
          <div>
            <img src="images/how--it-done-3.jpg">
          </div>          
          <h3>Wir kommen zu Ihnen</h3>
          <div class="service-content">
            <p>Egal, wo Sie sich befinden - zu Hause, bei der Arbeit oder im Hotel - unsere Ärzte besuchen Sie.</p>
          </div>
        </div>
        <!-- /bgrid --> 
        
      </div>
      <!-- /service-list --> 
      
    </div>
    <!-- /twelve --> 
    
  </div>
  <!-- /row -->
  <p class="scroll-btn">&nbsp;</p>
</section>
<!-- /services --> 

<!-- Doctor Section 
   ================================================== -->
<section id="doctors"> 
  
  <!-- doctor
   	=================================================== -->
  <div id='team'>
    <div class="row about-content">
      <div class="twelve twelve-bg columns">
        <div class="row">
             <div class="tab-whole six columns">
               
             </div>
             <div class="tab-whole six columns">
                <h3>Über Uns</h3>
                <p>Wir füllen die Lücke zwischen Praxisbesuch mit langen Wartezeiten und dem ärztlichen Bereitschaftsdienst. Gegen eine Gebühr von nur 5€ besuchen wir Sie unmittelbar zu Hause, wenn es sich nicht um einen Notfall handelt. 
                </p>
                <p>Unsere Kunden schätzen die Zeitersparnis und Bequemlichkeit unseres Services. Wir sind rund um die Uhr für Sie erreichbar und versorgen Sie mit notwendiger medizinischer Hilfe auch am Wochenende, an Feiertagen und außerhalb regulärer Sprechzeiten.</p>
                <p>Normalerweise dauert es 2-3 Stunden bis einer unserer Ärzte bei Ihnen eintrifft.</p>
             </div>

          </div>
        <!-- /twelve -->   
      </div>
      <!-- /team-wrapper -->   
    </div>
    <!-- /row --> 
    
  </div>
  <!-- /team -->
</section>
<!-- /about --> 

<!-- Support Section
   ================================================== -->
<section id="support">
  <div class="row contact section-head">
    <div class="twelve columns">
      <h2>Häufig gestellte Fragen</h2>
    </div>
  </div>
  <div class="row section-head">

    <div class="twelve columns">
      <ul>
        <li>
          <h4>Sind Sie auch bei mir in der Nähe?</h4>
          <p>Durch unser weites Ärztenetzwerk decken wir nahezu jeden Ort in Deutschland ab. Wenn Sie uns kontaktieren, fragen wir Sie nach Ihrer Postleitzahl und prüfen, ob unser Service für Sie verfügbar ist. </p>
        </li>
        <li>
          <h4>Zu welchen Zeiten ist der Service verfügbar?</h4>
          <p>Wir sind erreichbar 24/7, 365 Tage im Jahr, um Sie schnellstmöglich wieder auf den Weg der Besserung zu bringen.</p>
        </li>  
        <li>
          <h4>Wieviel kostet der Service? Übernimmt die Krankenkasse die Kosten?</h4>
          <p>Der Großteil der Kosten wird durch alle gesetzlichen Krankenkassen übernommen. Wenn Sie privat versichert sind, informieren Sie sich bitte, welche Kosten übernommen werden. Zusätzlich zu den von der Krankenkasse übernommenen Kosten berechnen wir einen Festbetrag von 5€. </p>
        </li> 
        <li>
          <h4>Wie bezahle ich für den Service?</h4>
          <p>Sie können direkt vor Ort bei Ihrem Arzt oder per Kreditkarte oder PayPal auf unserer Webseite bezahlen.</p>
        </li>                       
      </ul>   
    </div>

  </div>

  <!-- /columns -->
  
  </div>
</section>
<!-- /support--> 
<!-- Doctor Section 
   ================================================== -->
<section id="newsLetter"> 
  <div class="row contact section-head">
    <div class="twelve columns">
      <h2>Warum Medlanes?</h2>
    </div>
  </div>
  <div class="row">
    <div class="six columns mob-whole"> 
      <img src="images/icons/service-item-5.png">
      <h2>Für maximale Bequemlichkeit</h2>
      <p>Warten Sie auf einen Arzttermin? Im Wartezimmer? Mit einem Hausbesuch durch Medlanes verlieren Sie keine Zeit.</p>
    </div>
    <div class="six columns mob-whole"> 
      <img src="images/icons/service-item-2.png">
      <h2>Auch in der Nacht</h2>
      <p>Wenn es sich nicht um einen Notfall handelt, ist es schwer medizinischen Rat nach 18 Uhr zu erhalten - nicht bei uns!</p>
    </div>
   </div>
   <div class="row">
    <div class="six columns mob-whole"> 
      <img src="images/icons/service-item-3.png">
      <h2>Überall verfügbar</h2>
      <p>Wir besuchen Sie im Büro, im Hotel und auf der Konferenz - für uns kein Problem!</p>
    </div>
    <div class="six columns mob-whole"> 
      <img src="images/icons/service-item-4.png">
      <h2>Sogar am Wochenende</h2>
      <p>Einen Arzt am Wochenende zu finden, ohne in der Notaufnahmen zu warten, ist nicht einfach. Mit Medlanes könnte es einfacher nicht sein.</p>
    </div>    
  </div>
</section>
<!-- /about --> 

<!-- Contact Section
================================================== -->
<section id="contact">
    <div class="row contact section-head">
      <div class="twelve columns">
        <h2>Kontakt</h2>
      </div>
    </div>
    <div class="row form-section">
      
      <div id="contact-form" class="twelve columns">

          <form name="contactForm" id="contactForm" method="post" action="">

          <fieldset>
            <div class="row">
             <!-- <div class="six columns mob-whole form-feilds">                         
                <input name="contactFname" type="text" id="contactFname" placeholder="Telefonnummer" value="" />   
                <input name="contactEmail" type="text" id="contactEmail" placeholder="Email" value="" />
                <input name="contactEmail" type="text" id="Betreff" placeholder="Betreff" value="" />                
                <textarea name="contactMessage"  id="contactMessage" placeholder="Ihre Nachricht" rows="6" cols="50" ></textarea> 
                <button class="submit full-width">Absenden</button>               
              </div>-->
              <div class="six columns mob-whole contactText"> 
                   <p>Klicken Sie auf den Button und füllen Sie das Kontakformular aus. Wir machen uns innerhalb von 10 Minuten auf den Weg zu Ihnen.</p>
<a href="contact.html" class="button full-width orange">Jetzt Arzt kontaktieren</a>   
                  <p>Haftungsauschluss: <strong>Unser Service ist kein Ersatz für medizinische Notfallversorgung.</strong> Bei einem lebensbedrohlichen Notfall rufen Sie Bitte sofort <strong>112!</strong></p>

                                
              </div>  
              <div class="six columns mob-whole"> 
                <img src="images/contact-doctor.jpg" />
              </div>           
            </div>
          </fieldset>
        </form> <!-- /contactForm -->

          <!-- message box -->
          <div id="message-warning"></div>
          <div id="message-success">
             <i class="fa fa-check"></i>Ihre Nachricht wurde gesendet. Vielen Dank!<br />
        </div>

       </div> <!-- /contact-form -->        

    </div> <!-- /form-section -->     

 </section>  <!-- /contact-->

<!-- Footer
   ================================================== -->
<footer>
  <div class="row">
    <div id="go-top"> <a class="smoothscroll" title="Back to Top" href="#hero"><span>Top</span><i class="fa fa-long-arrow-up"></i></a> </div>
    <ul>
      <li><a href="index.html">Home</a></li>
      <li><a href="terms.html">AGBs</a></li>
      <li><a href="privacy.html">Datenschutz</a></li>
      <li><a href="contact.html">Kontakt</a></li>
    </ul>
  </div>
  <!-- /row --> 
  
</footer>
<!-- /footer --> 

<!-- Java Script
   ================================================== --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="js/global/jquery-1.10.2.min.js"><\/script>')</script> 
<script type="text/javascript" src="js/global/jquery-migrate-1.2.1.min.js"></script> 
<script src="js/plugins/fittext/jquery.fittext.js"></script> 
<script src="js/plugins/backstretch/backstretch.js"></script> 
<script src="js/plugins/waypoints/waypoints.js"></script> 
<script src="js/global/main.js"></script> 
<script src="js/plugins/wow/wow.min.js"></script> 
<script src="js/plugins/bxslider/jquery.bxslider.min.js"></script>
<script src="js/Api.js"></script>
<script>
	new WOW().init();
$( document ).ready( function( ) {
    $(document).ready(function(){
      $('.slider1').bxSlider({
      slideWidth: 300,
      minSlides: 3,
      maxSlides: 3,
      moveSlides: 1,
      slideMargin: 20
      });
  });
  var token = localStorage.getItem( 'token' );
  var is_dummy = localStorage.getItem( 'token_is_dummy' );
  if( !!token ) {
    //location.assign( 'your-stream.html' );
  }
  else {
    // first time
    api.UserDummy( function( resp ){
      localStorage.setItem( 'token', resp.result.token );
      localStorage.setItem( 'token_is_dummy', true );
      sessionStorage.setItem( 'first_time', true );
    } );
  }
  $( "#firstForm" ).submit( function( e ){
    e.preventDefault();
    api.CheckMail( function( resp ) {
        if( resp.success === true ) {
          // update the stored dummy token in the api lib
          api.setToken( localStorage.getItem( "token" ) );
          var email = $( "#email" ).val( );
          var question = $( "#question_text" ).val( );
          sessionStorage.setItem( 's_email', email );
          sessionStorage.setItem( 's_q_text', question );
          location.assign( 'additional.html' );
        }
        else {
          $( "#email" ).addClass( "input-error" );
        }
      } );
  } );

  network= getURLParameter('network');
  adposition= getURLParameter('adposition');
  devicemodel= getURLParameter('devicemodel');
  matchtype= getURLParameter('matchtype');
  keyword= getURLParameter('keyword');
  creative= getURLParameter('creative');

  // session store the params
  sessionStorage.setItem("network", network);
  sessionStorage.setItem("adposition", adposition);
  sessionStorage.setItem("devicemodel", devicemodel);
  sessionStorage.setItem("matchtype", matchtype);
  sessionStorage.setItem("keyword", keyword);
  sessionStorage.setItem("creative", creative);
  sessionStorage.setItem("funnelURL", document.URL);

 (function(){ 
        

        // $("#hero").css("backgroundImage", "url('images/country/" + realCity + ".jpg')");
        // $(".flex-caption .country").replaceWith("<span>&nbsp;"+ realCity + "</span>");
    }());

} );
</script>
<script src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js" type="text/javascript"></script>
  <script type="application/javascript">
    geoip2.city( function(geoipResponse){
      localStorage.setItem('realcountry',geoipResponse.country.names.en);
      localStorage.setItem('realcity', geoipResponse.city.names.en);

       
    }, function(error){
      console.log('Error from maxmind api: ',error);
      return;
    } );
  </script>

</body>
</html>
