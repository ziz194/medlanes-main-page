function Api( token ) {
	var api_url = "https://api.medlanes.com/index.php?/api_1/call/json";
	var company = "medlanes";

	function Call( action, callback, params, token ) {
		var request =  { action: action };
		if( !!params ) {
			request.parameters = params;
		}
		if( !!token ) {
			request.auth = {
				token: token
			};
		}
		var payload = { request: JSON.stringify( request ), type: 'Call' };
				//alert('req' + JSON.stringify(payload));

	    $.ajax( {
	        type: "POST",
	        url: api_url,
	        data: payload,
	        dataType: 'json',
	        async: true
	    } )
	    .done( function(resp){
	    			//alert('res' + JSON.stringify(resp));

	    	callback(resp);
	    } )
		.fail( console.log );

	}

	return {
		TransactionFinish: function( callback, ticket_id ) {
			Call( "transaction_finish", callback, { id: ticket_id, funnel: 'med', company: company, amount: '25' }, token );
		},
		setToken: function( _token ) {
			token = _token;
		},
		UpdateUser: function ( callback ) {

			var firstname = $( "#first-name" ).val( );
			var lastname = $( "#last-name" ).val( );
			var password = $( "#password" ).val( );
			var age = $( "#age" ).val( );
			var gender = $( "[name^='gender']:checked" ).val( );
			//var email = $( "#email" ).val( );

			switch( gender ) {
				case 'male':
					gender = 'm';
				break;
				case 'female':
					gender = 'f';
				break;
				default:
					gender = '';
				break;
			}

			var params = {
		//		email: email,
				firstname: firstname,
				lastname: lastname,
				age: age,
				gender: gender
			};

			if( !!password ) {
				params.password = password;
			}

			Call( "user_update", callback, params, token );
		},
		Signup: function ( callback, email, type ) {


			if( type === 'continue' ) {
				var firstname = $( "#name" ).val( );		
				var age = $( "#age" ).val( );
				var gender = $( "#gender" ).val( );
				switch( gender ) {
					case 'male':
						gender = 'm';
					break;
					case 'female':
						gender = 'f';
					break;
					default:
						gender = '';
					break;
				}
				var params = {
					email: email,
					firstname: firstname,
					age: age,
					gender: gender
				};
			}
			else {
				var params = {
					email: email					
				};
			}			

			Call( "user_signup", callback, params, token );
		},
		CheckMail: function ( callback ) {

			var email = $( "#email" ).val( );

			Call( "check_mail", callback, { email: email } );
		},
		Login: function ( callback ) {

			var email = $( "#email" ).val( );
			var password = $( "#password" ).val( );

			Call( "login", callback, { email: email, password: password } );
		},
		Me: function ( callback ) {
			Call( "me", callback, null, token );
		},
		UserDummy: function ( callback ) {
			Call( "user_dummy", callback );
		},
		QuestionList: function ( callback ) {
			Call( "question_list", callback, null, token );
		},
		QuestionDetails: function ( callback, id ) {
			Call( "question_details", callback, { id: id }, token );
		},
		MessageAdd: function ( callback, to_ticket ) {

			var message_text = $( "#message-text" ).val( );

			var params = {
				text: message_text,
				idTicket: to_ticket
			};

			Call( "message_add", callback, params, token );
		},
		QuestionAdd: function ( callback, question_text ) {

			var text_tried = $( "#text_tried" ).val( );

			var params = {
				text: question_text,
				state: 'stored',
				company: 'docreply',
				funnel: 'docreply',
				sendEmail: 'true',
				message_properties: [
					{ name: "tried", value: text_tried }
				]			
			};

			Call( "question_add", function( data ) {
				callback( data );				
			}, params, token );
		},
		AdditionalData: function( callback, ticket ) {
		  var network=  sessionStorage.getItem("network");
		  var adposition=   sessionStorage.getItem("adposition");
		  var devicemodel=  sessionStorage.getItem("devicemodel");
		  var matchtype=   sessionStorage.getItem("matchtype");
		  var keyword=   sessionStorage.getItem("keyword");
		  var creative=   sessionStorage.getItem("creative");
		  var funnelURL = sessionStorage.getItem("funnelURL");

		  var token = localStorage.getItem('token');
		  //var ticket = localStorage.getItem('specmsgid');
		  var rcountry = localStorage.getItem('realcountry');
		  var rcity = localStorage.getItem('realcity');
		  var userLang = navigator.language || navigator.userLanguage; 

		  Call( "additional_data", callback, {"idTicket":ticket,"data":[{"funnelCountry":'US'},{"funnelLanguage":'en'},{"realCountry":rcountry},{"realLanguage":userLang},{"realCity":rcity},{"device":'device'},{"specialization":''},{"funnel":'docreply-livechat'},{"page":'docreply-livechat'},{"network":network},{"adposition":adposition},{"devicemodel":devicemodel},{"matchtype":matchtype},{"keyword":keyword},{"AD-ID":creative},{"funnelURL":funnelURL}]}, token );
		}
	};
}

var token = '';
var api;
var image_server_url = 'https://api.medlanes.com/';

$(document).ready( function( ){
	token = localStorage.getItem( 'token' );
	api = Api( token );


} );

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}



